package net.niyubyb.android.retrofitlite;

import java.util.List;

/**
 * Created by Usuario on 03/02/2018.
 */
public class Course {
    public String title;
    public String subtitle;
    public List<Instructor> instructors;
}
