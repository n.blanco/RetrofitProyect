package net.niyubyb.android.retrofitlite;

import retrofit.Call;
import retrofit.http.DELETE;
import retrofit.http.GET;
import retrofit.http.PUT;

/**
 * Created by Usuario on 03/02/2018.
 */
public interface Service {
    public static final String BASE_URL = "https://www.udacity.com/public-api/v0/";
    @GET("courses")
    Call<Catalogo> listaCatalogo();
    @DELETE("courses")
    Call<Catalogo> listaCatalogo2();
}
