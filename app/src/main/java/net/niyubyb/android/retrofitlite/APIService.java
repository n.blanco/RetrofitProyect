package net.niyubyb.android.retrofitlite;

import retrofit.Call;
import retrofit.http.DELETE;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.PUT;
import retrofit.http.Path;

public interface APIService {
    @POST("/posts")
    @FormUrlEncoded
    Call<Post> savePost(@Field("title") String title,
                        @Field("body") String body,
                        @Field("userId") long userId);
    @PUT("/posts/{id}")
    @FormUrlEncoded
    Call<Post> updatePost(@Path("id") String id,
                          @Field("title") String title,
                          @Field("body") String body,
                          @Field("userId") long userId);
    @DELETE("/posts/{id}")
    Call<Post> deletePost(@Path("id") String id);


}
