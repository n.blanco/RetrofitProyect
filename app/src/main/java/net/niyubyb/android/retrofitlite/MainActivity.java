package net.niyubyb.android.retrofitlite;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import retrofit.Call;
import retrofit.Callback;
import retrofit.GsonConverterFactory;
import retrofit.Response;
import retrofit.Retrofit;

public class MainActivity extends AppCompatActivity {
    private TextView mResponseTv;
    private TextView mResponseTv2;
    private APIService mAPIService;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final EditText titleEt = (EditText) findViewById(R.id.et_title);
        final EditText bodyEt = (EditText) findViewById(R.id.et_body);
        Button submitBtn = (Button) findViewById(R.id.btn_submit);
        Button submitBtn2 = (Button) findViewById(R.id.btn_submit2);
        mResponseTv = (TextView) findViewById(R.id.tv_response);
        mResponseTv2 = (TextView) findViewById(R.id.tv_response2);
        mAPIService = ApiUtils.getAPIService();

        submitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String title = titleEt.getText().toString().trim();
                String body = bodyEt.getText().toString().trim();

                if(!TextUtils.isEmpty(title) && !TextUtils.isEmpty(body)) {
                    sendPost(title, body);

                }
            }
        });

        submitBtn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Retrofit retrofit  = new Retrofit.Builder().baseUrl(Service.BASE_URL).
                        addConverterFactory(GsonConverterFactory.create()).build();

                Service service = retrofit.create(Service.class);
                retrofit.Call<Catalogo> requesCatalogo = service.listaCatalogo();

                requesCatalogo.enqueue(new Callback<Catalogo>() {
                    @Override
                    public void onResponse(Response<Catalogo> response, Retrofit retrofit) {
                        if (!response.isSuccess()){
                            Log.i("TAG", "Error" + response.code());
                        }else {
                            Catalogo catalogo = response.body();

                            for (Course c : catalogo.courses){
                                //showResponse2(String.format("%s %s", c.title, c.subtitle));
                                //showResponse2(String.format("%s: %s", c.title, c.subtitle));
                                 showResponse2(response.raw().toString());
                                Log.i("TAG", String.format("%s: %s", c.title, c.subtitle));

                                //showResponse2((response.headers().toString()));

                                for (Instructor i : c.instructors){
                                    Log.i("TAG", i.name);

                                }
                                Log.i("TAG", "-------------");

                            }
                        }

                    }

                    @Override
                    public void onFailure(Throwable throwable) {
                        Log.e("TAG", "Error: " + throwable.getMessage());
                    }



                });
            }
        });

    }

    public void sendPost(String title, String body) {
        mAPIService.savePost(title, body, 1).enqueue(new Callback<Post>() {
            @Override
            public void onResponse(Response<Post> response, Retrofit retrofit) {
                if(response.isSuccess()) {
                    showResponse(response.body().toString());
                    Log.i("TAG", "post submitted to API." + response.body().toString());
                }
            }

            @Override
            public void onFailure(Throwable throwable) {
                Log.e("TAG", "Unable to submit post to API.");
            }

        });
    }

    public void showResponse(String response) {
        if(mResponseTv.getVisibility() == View.GONE) {
            mResponseTv.setVisibility(View.VISIBLE);
        }
        mResponseTv.setText(response);
    }
    public void showResponse2(String response2) {
        if(mResponseTv2.getVisibility() == View.GONE) {
            mResponseTv2.setVisibility(View.VISIBLE);
        }
        mResponseTv2.setText(response2);
    }
}
